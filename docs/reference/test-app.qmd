# test-app { #src.test-app }

`test-app`



## Functions

| Name | Description |
| --- | --- |
| [main](#src.test-app.main) | Function prints an argument. |
| [sum_two_numbers](#src.test-app.sum_two_numbers) | Function calculate a sum of two numbers |

### main { #src.test-app.main }

`test-app.main(string)`

Function prints an argument.

#### Parameters

| Name     | Type   | Description          | Default    |
|----------|--------|----------------------|------------|
| `string` | str    | String for printing. | _required_ |

### sum_two_numbers { #src.test-app.sum_two_numbers }

`test-app.sum_two_numbers(a, b)`

Function calculate a sum of two numbers

#### Parameters

| Name   | Type   | Description   | Default    |
|--------|--------|---------------|------------|
| `a`    | int    | First number  | _required_ |
| `b`    | int    | Second number | _required_ |

#### Returns

| Type   | Description   |
|--------|---------------|
| int    | Sum           |