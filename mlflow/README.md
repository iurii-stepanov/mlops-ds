# MLflow
Пример использования MLflow

## Запуск приложений

1. Склонировать репозитирий

    ```bash
    git clone https://gitlab.com/iurii-stepanov/mlops-ds.git
    cd mlops-ds/mlflow/docker-compose
    ```


2. Запустить приложения

    ```bash
    docker-compose up -d --build
    ```

3. Приложения доступны по ссылкам:

    - MLflow UI: http://localhost:8080

    - MinIO UI: http://localhost:9000

## Запуск демонстрации
Запустить ноутбук `mlflow/titanic.ipynb`
В результате в MLflow залогируется эксперимент из двух запусков с разными моделями:
- Logistic Regression 
- Random Forest Classifier

Также залогируются сами модели и другие артефакты.


## Сравнение результатов
Результаты двух запусков представлены на сриншотах.

Учитывая демонстрационный характер исследования, разница между Logistic Regression и Random Forest Classifier получилась несущественная, но все же выбрать стоит Random Forest Classifier, т.к. f1-score чуть выше (0.77 против 0.76)

![runs](/mlflow/pics/runs.png)

![comparison](/mlflow/pics/comparison.png)
