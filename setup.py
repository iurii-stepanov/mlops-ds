from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="Educational project for the course MLOps and production in DS research 3.0",
    author="Iurii Stepanov",
    license="MIT",
)
