# from datetime import datetime

# import numpy as np
# import pandas as pd
import s3_utils

def transform_ver_1(dt, **context):
    df = s3_utils.read_from_csv("titanic_raw_data", dt)
    df = df.drop(columns=["PassengerId", "Name", "Sex", "Ticket", "Cabin", "Embarked"])
    s3_utils.save_as_csv("transform_ver_1", dt, df, branch=f"transform_ver_1_{dt}")

def transform_ver_2(dt, **context):
    df = s3_utils.read_from_csv("titanic_raw_data", dt)
    df = df.drop(columns=["Name", "Sex", "Ticket", "Cabin", "Embarked"])
    s3_utils.save_as_csv("transform_ver_2", dt, df, branch=f"transform_ver_2_{dt}")