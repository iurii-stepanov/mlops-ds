from datetime import datetime #, timedelta

import pandas as pd
import s3_utils
import requests
from io import StringIO

def upload_raw_data():
    url = "https://www.dropbox.com/scl/fi/qxe7ipdjt00outb4wy22a/titanic_train.csv?rlkey=33040a9vjnniszstep0bften1&dl=1"
    response = requests.get(url)
    if response.status_code == 200:
        content = response.content.decode('utf-8')
        df = pd.read_csv(StringIO(content))
    else:
        print(f"Error during download file. Response code: {response.status_code}")
    dt = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    s3_utils.save_as_csv("titanic_raw_data", dt, df)
    return dt