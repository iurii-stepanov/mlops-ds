from datetime import datetime, timedelta

import upload
import transform
from airflow.operators.python import PythonOperator
from lakefs_client.model.reset_creation import ResetCreation
from lakefs_provider.hooks.lakefs_hook import LakeFSHook
from lakefs_provider.operators.commit_operator import LakeFSCommitOperator
from lakefs_provider.operators.create_branch_operator import \
    LakeFSCreateBranchOperator
from lakefs_provider.operators.merge_operator import LakeFSMergeOperator

from airflow import DAG



commit_metadata = {
    "dag_version": "1.0",
    "transform_version": "1.0",
    "upload_raw_data_version": "1.0",
}

def _transform_steps(dt_arg, transform_name, transform_func):
    branch = f"{transform_name}_{dt_arg}"

    def _reset_branch(context):
        ti = context["ti"]
        dt = ti.xcom_pull(task_ids='extract', dag_id='etl', key='return_value')
        branch = f"{transform_name}_{dt}"
        lakefs_hook = LakeFSHook(lakefs_conn_id="lakefs")
        print("XXXDX" + branch)
        lakefs_hook.get_conn().branches.reset_branch(repository="lakefs-airflow-repo", branch=branch,
                                                     reset_creation=ResetCreation(type="reset"))

    create_branch_op = LakeFSCreateBranchOperator(
        task_id=f"{transform_name}_create_branch",
        repo="lakefs-airflow-repo",
        branch=branch,
        lakefs_conn_id="lakefs",
        source_branch="main",
    )

    transform_op = PythonOperator(task_id=f"{transform_name}_transform",
                                          python_callable=transform_func,
                                          op_kwargs={"dt": dt_arg},
                                          retries=5,
                                          retry_delay=timedelta(seconds=5),
                                          on_retry_callback=_reset_branch)

    commit_op = LakeFSCommitOperator(
        task_id=f"{transform_name}_commit",
        repo="lakefs-airflow-repo",
        branch=branch,
        lakefs_conn_id="lakefs",
        msg="Transform result",
        metadata=commit_metadata,
    )

    merge_op = LakeFSMergeOperator(
        task_id=f"{transform_name}_merge",
        repo="lakefs-airflow-repo",
        lakefs_conn_id="lakefs",
        source_ref=branch,
        destination_branch="main",
        msg="Merge transform result",
        metadata=commit_metadata,
    )
    return (merge_op << commit_op << transform_op << create_branch_op)


with DAG(
    dag_id="etl",
    catchup=False,
    schedule="@daily",
    start_date=datetime(2024, 1, 1),
    tags=["produces", "dataset-scheduled"],
) as dag1:
    t1 = PythonOperator(task_id="upload_raw_data", python_callable=upload.upload_raw_data)
    t2 = LakeFSCommitOperator(
        task_id="commit_raw_data",
        repo="lakefs-airflow-repo",
        branch="main",
        lakefs_conn_id="lakefs",
        msg="Upload raw data",
        metadata=commit_metadata,
    )
    t3 = _transform_steps(t1.output, "transform_ver_1",
                          transform.transform_ver_1)
    t4 = _transform_steps(t1.output, "transform_ver_2",
                          transform.transform_ver_2)
    t1 >> t2 >> [t3, t4]
