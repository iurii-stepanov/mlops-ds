mlops-ds
==============================

Пример использования LakeFS совместно с Airflow

## 1. Запуск и остановка приложений
Клонирование репозитория:
```bash
git clone https://gitlab.com/iurii-stepanov/mlops-ds.git
cd mlops-ds/lakefs
```
Сборка docker-образа Airflow c установкой библиотеки lakefs_client (выполняется один раз после изменения `lakefs/Dockerfile`):
```bash
docker-compose build
```
Запуск приложений:
```bash
docker-compose up -d
```
Остановка приложений с удалением дисков:
```bash
docker-compose down --volumes
```

## 2. DAG
В демонстрационном DAG следующие шаги:
- `upload_raw_data`: загрузка "сырых" данных из DropBox в LakeFS
- `commit_raw_data`: коммит "сырых" данных в main
- `transform_ver_X_create_branch`: создание новой ветки в LakeFS
- `transform_ver_1_transform`: обработка и сохранение данных в новой ветке
- `transform_ver_1_commit`: коммит обработанных данных в соответствующую ветку
- `transform_ver_1_merge`: слияние в ветку `main`

## 3. Проверка работы
1. Открыть интерфейс Airflow: http://localhost:8080/
    - Логин: airflow
    - Пароль: airflow
2. Запустить DAG `etl`, убедиться, что DAG выполнился без ошибок
3. Перейти в LakeFS: http://localhost:8000/ 
    - Access Key: AKIAIOSFOLKFSSAMPLES
    - Secret Key: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
    
Убедиться, что появились следующие сущности:
  - репозиторий: lakefs-airflow-repo
  - ветки: `main`, `transform_ver_1_{date_time}` и `transform_ver_2_{date_time}`
  - в ветке main находятся директории  `titanic_raw_data`,  `transform_ver_1` и ` transform_ver_2` с соответствующими файлами в них ("сырые" данные и два варианта обработки данных)
