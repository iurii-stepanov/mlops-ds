Работа с репозиторием mlops-ds
==============================


## 1. Структура проекта
Структура подготовлена с использованием [Cookiecutter Data Science](https://drivendata.github.io/cookiecutter-data-science/). Описание структуры:

------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


## 2. Как работать с репозиторием

- Репозиторий ведется по Github-flow;
- Разработка ведется в отдельных ветках;
- Пуш в master только через merge-request и ревью;
- Перед коммитом обязательно проверить код с помощью pre-commit c подключенным Ruff (см. ниже);
- После коммита код будет проверен в пайплайне с помощью тех же pre-commit и Ruff;
- Если автоматические проверки провалились, исправьте код согласно выводу в консоли пайплайна и повторите пуш.

## 3. Pre-commit
Для проверки кода перед пушем используйте [pre-commit](https://pre-commit.com/) с подключенным [Ruff](https://docs.astral.sh/ruff/). Необходимая конфигурация находится в файле `.pre-commit-config.yaml` в корне проекта.

- Установите `pre-commit` и `Ruff`:
```bash
pip install pre-commit
pip install ruff
```
- Из директории проекта запустите `pre-commit`:
```bash
pre-commit run --all-files --fix
```
