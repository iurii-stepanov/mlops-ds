import time


def main(string: str) -> None:
    """Function just prints an its argument

    Args:
        string (str): String for printing
    """
    print(string)
    print("Waiting for 1 minute...")
    time.sleep(60)
    print("Done waiting!")


def sum_two_numbers(a: int, b: int) -> int:
    """Function calculates a sum of two numbers

    Args:
        a (int): First number
        b (int): Second number

    Returns:
        int: Sum of two numbers
    """
    return a + b


if __name__ == "__main__":
    main("Hello Word!")
