import pickle
import sys

import pandas as pd
from hydra import compose, initialize
from sklearn.ensemble import RandomForestClassifier


def train_model(input_file, output_file):
    initialize(config_path="../")
    cfg = compose(config_name="config")

    df = pd.read_csv(input_file)
    X = df.drop(columns=["Survived"])
    y = df["Survived"]

    num_cpu = max(1, int(cfg.train.num_cpu) // 2)

    model = RandomForestClassifier(
        max_depth=cfg.train.v2.max_depth,
        n_estimators=cfg.train.v2.n_estimators,
        n_jobs=num_cpu,
    )
    model.fit(X, y)

    with open(output_file, "wb") as f:
        pickle.dump(model, f)


if __name__ == "__main__":
    train_model(sys.argv[1], sys.argv[2])
