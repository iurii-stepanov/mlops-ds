import sys

import pandas as pd
from hydra import compose, initialize


def preprocess(input_file, output_file):
    initialize(config_path="../")
    cfg = compose(config_name="config")

    df = pd.read_csv(input_file)
    df = df.drop(columns=cfg.preprocessing.v1.drop_columns)
    df.to_csv(output_file, index=False)


if __name__ == "__main__":
    preprocess(sys.argv[1], sys.argv[2])
