FROM python:3.9

RUN pip install poetry

WORKDIR /app

COPY ./pyproject.toml ./

RUN poetry install --without dev

COPY ./src ./

ENTRYPOINT [ "python", "test-app.py" ]
