mlops-ds
==============================

Educational project for the course MLOps and production in DS research 3.0

## 1. Docker
Сборка docker-образа:
```bash
docker build . -t test-app:v1
```
Запуск контейнера в интерактивном режиме:
```bash
docker run -it test-app:v1 /bin/bash
```
Остановка контейнера:
```bash
docker stop test-app:v1
```
Удаление контейнера:
```bash
docker rm test-app:v1
```
Удаление образа:
```bash
docker rmi test-app:v1
```


## 2. Snakemake

В целях изучения инструментов автоматизации исследований подготовлен паплайн, состоящий из трех шагов:
1. **Загрузка данных** (датасет titanic загружается c DropBox)
2. **Обработка данных** (два варианта)
3. **Обучение модели** (два варианта)

Комбинации шагов обработки и обучения подготавливают 4 модели (см. DAG на рисунке ниже)

![DAG](./docs/images/dag.png "DAG")
